#!/usr/bin/perl

use strict;
use warnings;
use File::Find;
use Data::Dumper;

my $num_args = $#ARGV + 1;
if ($num_args != 1) {
    print "Missing parameter. Usage: TestSuiteRunner.pl <test_directory_path>\n";
    exit 1;
}

my $testDirectoryPath=$ARGV[0];
chomp($testDirectoryPath);

if (-e $testDirectoryPath) {
    if (! -d $testDirectoryPath) {
    	print "test_directory_path '$testDirectoryPath' is not a directory\n";
    	exit 1;
    }
} else {
    print "test_directory_path '$testDirectoryPath' does not exist\n";
    exit 1;
}
my $yafshunitBinaryDirectory = `dirname "$0"`;
chomp($yafshunitBinaryDirectory);
my $singleTestFileAnalyserBinary = "$yafshunitBinaryDirectory/SingleTestFileAnalyser.sh";
my $singleTestCaseRunnerBinary = "$yafshunitBinaryDirectory/SingleTestCaseRunner.sh";
my $statusLoggerBinary = "$yafshunitBinaryDirectory/StatusLogger.sh";

if (! -f $singleTestFileAnalyserBinary) {
    	print "singleTestFileAnalyserBinary not found under '$singleTestFileAnalyserBinary'\n";
    	exit 1;
}
if (! -f $singleTestCaseRunnerBinary) {
    	print "singleTestCaseRunnerBinary not found under '$singleTestCaseRunnerBinary'\n";
    	exit 1;
}
if (! -f $statusLoggerBinary) {
    	print "statusLoggerBinary not found under '$statusLoggerBinary'\n";
    	exit 1;
}
`chmod u+x "$singleTestFileAnalyserBinary"`;
`chmod u+x "$singleTestCaseRunnerBinary"`;
`chmod u+x "$statusLoggerBinary"`;
if (! -X $singleTestFileAnalyserBinary) {
    	print "singleTestFileAnalyserBinary '$singleTestFileAnalyserBinary must be executable'\n";
    	exit 1;
}
if (! -X $singleTestCaseRunnerBinary) {
    	print "singleTestCaseRunnerBinary '$singleTestCaseRunnerBinary must be executable'\n";
    	exit 1;
}
if (! -X $statusLoggerBinary) {
    	print "statusLoggerBinary '$statusLoggerBinary' must be executable\n";
    	exit 1;
}

my @testFiles;
find(sub { push (@testFiles, $File::Find::name) if -f $_ && $_ =~ /Test.sh$/ ; }, $testDirectoryPath );

my $analysedTests={};
foreach (@testFiles) {
    my $testMethodNamesAsSemicolonSeparatedString=`$singleTestFileAnalyserBinary $_`;
    $analysedTests->{$_} = $testMethodNamesAsSemicolonSeparatedString;
}

my $tainted = 0;
foreach my $testFile (keys $analysedTests) {
    my @testMethodNames = split /;/, $analysedTests->{$testFile};
    $analysedTests->{$testFile}={};
    foreach my $testMethod (@testMethodNames) {
	    $analysedTests->{$testFile}->{$testMethod}={};
            my $potentialErrorMessage=`$singleTestCaseRunnerBinary $testFile $testMethod`;
	    chomp($potentialErrorMessage);
	    my $returnCode = $? >> 8;
	    if ($returnCode == 0) {
		system("$statusLoggerBinary 'PASSED' '$testFile#$testMethod'");
		$analysedTests->{$testFile}->{$testMethod}{'result'}="success";
	    } else {
		system("$statusLoggerBinary  'FAILED' '$testFile#$testMethod'");
		$analysedTests->{$testFile}->{$testMethod}{'result'}="failed";
		$analysedTests->{$testFile}->{$testMethod}{'errorMessage'}=$potentialErrorMessage;
        $tainted = 1;
	    }
    }
}
if($tainted) {
    exit 1;
} else {
    exit 0;
}

#print Dumper($analysedTests);

#!/bin/sh
set -e

cd "`dirname $0`"
absoluteTestDir="`pwd`"

for file in `find "${absoluteTestDir}" -type f -name "*Test.sh"`; do
    echo "===> Running test $file"
    chmod 700 "$file"
    cd "`dirname \"$file\"`"
    ./`basename "$file"`
done

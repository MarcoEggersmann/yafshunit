#!/bin/sh
set -e
set -u

originalDirectory=`pwd`
relativeProjectRootDirectory=`dirname "$0"`
cd "$relativeProjectRootDirectory"
absoluteProjectRootDirectory=`pwd`

echo "===> Cleaning build directory"
rm -rf "$absoluteProjectRootDirectory/target"

set +u
if [ "$1" == "clean" ]; then exit 0; fi
set -u

echo "===> preparing build directory"
mkdir "$absoluteProjectRootDirectory/target"

echo "===> Running tests"
"$absoluteProjectRootDirectory"/src/test/AllTests.sh

echo "===> Creating release tarball"
cp -rp "$absoluteProjectRootDirectory"/src/main "$absoluteProjectRootDirectory"/target/yafshunit
cd "$absoluteProjectRootDirectory"/target
versionNumber="0.1"
tar -czf yafshunit_${versionNumber}.tar.gz yafshunit

echo "===> Signing release tarball"
gpg --armor --detach-sign yafshunit_${versionNumber}.tar.gz

cd "$originalDirectory"
